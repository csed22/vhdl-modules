library ieee;
use ieee.std_logic_1164.all;


entity StepCounter is
	port(
		X 	: in std_logic;
		Clk : in std_logic;
		Z 	: out std_logic_vector(3 downto 0) := "UUUU"
	);
end	entity;

architecture rtl of StepCounter is
	
	signal Z_memory : std_logic_vector(3 downto 0) := "0000"; --

begin
	
	Clocked : process(Clk) is
		
	begin
	
        if rising_edge(Clk) then
 
 			Z_memory(3) <= (not(X) and Z_memory(3) and not(Z_memory(1))) or 
 						   (X and Z_memory(3) and Z_memory(0)) or
 						   (Z_memory(3) and Z_memory(1) and not(Z_memory(0))) or
 						   (X and Z_memory(3) and Z_memory(2)) or
 						   (not(X) and Z_memory(3) and not(Z_memory(2))) or
 						   (X and not(Z_memory(3)) and not(Z_memory(2)) and not(Z_memory(1)) and not(Z_memory(0))) or
 						   (not(X) and not(Z_memory(3)) and Z_memory(2) and Z_memory(1) and Z_memory(0))
 						   ;
 						   
 			Z_memory(2) <= (not(X) and Z_memory(2) and not(Z_memory(1))) or 
 						   (X and Z_memory(2) and Z_memory(0)) or
 						   (Z_memory(2) and Z_memory(1) and not(Z_memory(0))) or
 						   (X and not(Z_memory(2)) and not(Z_memory(1)) and not(Z_memory(0))) or
 						   (not(X) and not(Z_memory(2)) and Z_memory(1) and Z_memory(0))
 						   ;
 			
 			Z_memory(1) <= X XOR Z_memory(1) XOR Z_memory(0);
 			
 			Z_memory(0) <= not(Z_memory(0));
 			 			
            end if;
            
    end process Clocked;
    
    Z <= Z_memory;
	
end architecture rtl;
