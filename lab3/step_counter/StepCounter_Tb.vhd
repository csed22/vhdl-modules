library ieee;
use ieee.std_logic_1164.all;

entity StepCounter_tb is
end	entity;

architecture Simulation of StepCounter_tb is
	
	constant testVectorWidth : integer := 30;
	
	-- high clock frequency to fit the simulation in ns
    constant ClockFrequencyHz : integer := 20e6; -- 20 MHz
    constant ClockPeriod : time := 1000 ms / ClockFrequencyHz;	-- 50ns
 
    signal Clk 	: std_logic := '1';
    signal X 	: std_logic := 'X';
    signal Z 	: std_logic_vector(3 downto 0); -- @suppress "signal Z is never read"
    
    signal Test	: std_logic_vector(testVectorWidth-1 downto 0) := "001110111111111111000000000000"; -- @suppress "signal Test is never written"

begin
	
	-- The Device Under Test (DUT)
	i_StepCounter : entity work.StepCounter
		port map(
			X   => X,
			Clk => Clk,
			Z   => Z
		);
 
    -- Process for generating clock
    Clk <= not Clk after ClockPeriod / 2;
 
    -- Testbench sequence
    Testbench : process is
	begin
		
		X <= Test(testVectorWidth-1);
		wait for 49 ns;
		
		for i in testVectorWidth-2 downto 0 loop
			
			X <= Test(i);
			wait for 50 ns;
			
		end loop;
		
		X <= '0';		
		wait;
	end process Testbench;
 
end architecture Simulation;
