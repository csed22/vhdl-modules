PROJECT_NAME="StepCounter"
PROJECT_NAME_TB="StepCounter_Tb"
WORKING_DIR="C:\Users\EvilS\eclipse-workspace\Lab_3"

cd $WORKING_DIR;
ghdl -a $PROJECT_NAME.vhd;
ghdl -a $PROJECT_NAME_TB.vhd;
ghdl -e $PROJECT_NAME_TB;
ghdl -r $PROJECT_NAME_TB --stop-time=2000ns --wave=simulation.ghw
gtkwave simulation.ghw;
