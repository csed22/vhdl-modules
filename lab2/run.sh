PROJECT_NAME="EvenDetector"
PROJECT_NAME_TB="EvenDetector_tb"

#Make sure to modify this value:
#WORKING_DIR="...\workspace\lab2"

cd $WORKING_DIR;
ghdl -a $PROJECT_NAME.vhd;
ghdl -a $PROJECT_NAME_TB.vhd;
ghdl -e $PROJECT_NAME_TB;
ghdl -r $PROJECT_NAME_TB --stop-time=500ns --wave=simulation.ghw
gtkwave simulation.ghw;
# cmd /k command

#! <3 -> https://www.taniarascia.com/how-to-create-and-use-bash-scripts/