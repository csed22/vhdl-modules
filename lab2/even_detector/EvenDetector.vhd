library ieee;
use ieee.std_logic_1164.all;


entity EvenDetector is
	port(
		X 	: in std_logic;
		Clk : in std_logic;
		Z 	: out std_logic := 'X'
	);
end	entity;

architecture rtl of EvenDetector is
	
	-- Enumerated type declaration and state signal declaration
	type t_State is (X0, X1, X2, O0, O1, O2, T0, T1, T2, Undefined);
	
	--  X no   ones
	--  O odd  ones
	--  T even ones
	
	--  0 no   zeros
	--  1 odd  zeros
	--  2 even zeros
		 
	signal State : t_State := X0; 
	signal first_time : boolean := true; 
	

begin
	
	ChangeState : process(Clk) is  -- @suppress "Incomplete sensitivity list. Missing signals: X, first_time"
		
	impure function TransferState( If_0  : t_State;
									o_0  : std_logic;
									If_1 : t_State;
									o_1  : std_logic) return t_State is
	begin
		
		if X = '0' then
			Z <= o_0;
			return If_0;
		elsif X = '1' then
			Z <= o_1;
			return If_1;
		else
			Z <= 'X';
			return Undefined;
		end if;

	end function TransferState;
		
	begin
	
	if first_time then
		
		first_time <= false after 30 ns;	-- long enough to just reach the falling edge.
		
		State <= TransferState(X1,'X', O0,'X');
		
	end if;

        if rising_edge(Clk) then
 
                case State is
                                   
                    when X0 => 
                    	State <= TransferState(X1,'0', O0,'0');
                    when X1 => 
                    	State <= TransferState(X2,'1', O1,'0');
                    when X2 => 
                    	State <= TransferState(X1,'0', O2,'1');
                    when O0 => 
                    	State <= TransferState(O1,'0', T0,'1');
                    when O1 => 
                    	State <= TransferState(O2,'1', T1,'1');
                    when O2 => 
                    	State <= TransferState(O1,'0', T2,'1');
                    when T0 => 
                    	State <= TransferState(T1,'1', O0,'0');
                    when T1 => 
                    	State <= TransferState(T2,'1', O1,'0');
                    when T2 => 
                    	State <= TransferState(T1,'1', O2,'1');
                    	
                    when Undefined =>
                    	report "Bad Input!, Starting again";
                    	Z 		<= 'X';
                    	State	<=  X0;
                    	
                 end case;
 
            end if;
    end process ChangeState;
	
end architecture rtl;
