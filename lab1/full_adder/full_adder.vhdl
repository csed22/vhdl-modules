library ieee;
use ieee.std_logic_1164.all;

entity full_adder is
    port(
	i_carry: in STD_LOGIC;
	i_1 : in STD_LOGIC;
	i_2 : in STD_LOGIC;
	o_sum : out STD_LOGIC;
	o_carry : out STD_LOGIC
	);
end full_adder;

architecture STRUCTURAL of full_adder is

	signal xor_1n2	  : std_logic;
	signal and_1n2	  : std_logic;
	signal customWire : std_logic;	

begin
	
	xor_1n2  	<= i_1 xor i_2;
	customWire  <= xor_1n2 and i_carry;
	and_1n2 	<= i_1 and i_2;
	
	o_sum   <= xor_1n2 xor i_carry;
	o_carry <= customWire or and_1n2;
	
end STRUCTURAL;