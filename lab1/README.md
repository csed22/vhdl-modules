```shell
ghdl -s foo.vhdl -> check syntax errors

ghdl -a foo.vhdl -> analyze file

ghdl -a foo_tb.vhdl

ghdl -e foo_tb

ghdl -r foo_tb

ghdl -r foo_tb --vcd=foo.vcd

gtkwave foo.vcd
```