library ieee;
use ieee.std_logic_1164.all;

entity ripple_adder is
    port(
	input_1 : in STD_LOGIC_VECTOR(3 downto 0);
    input_2 : in STD_LOGIC_VECTOR(3 downto 0);
	o_sum 	: out STD_LOGIC_VECTOR(3 downto 0);		--result
	o_carry : out STD_LOGIC							--carry result
	);
end ripple_adder;

architecture STRUCTURAL of ripple_adder is

	component full_adder is
		port( 
		i_carry: in STD_LOGIC;
		i_1 : in STD_LOGIC;
		i_2 : in STD_LOGIC;
		o_sum : out STD_LOGIC;
		o_carry : out STD_LOGIC
		);
	end component full_adder;

	signal w_CARRY : std_logic_vector(4 downto 0);	--allcarrys
	signal w_SUM : std_logic_vector(3 downto 0);
	
begin

	w_CARRY(0) <= '0';
	
	SET_WIDTH : for ii in 0 to 3 generate
		i_FULL_ADDER_INST : full_adder
		port map(
			i_carry => w_CARRY(ii),
			i_1 	=> input_1(ii),
			i_2 	=> input_2(ii),
			o_sum 	=> w_SUM(ii),
			o_carry => w_CARRY(ii+1)
		);
		
	end generate SET_WIDTH;
	
	o_sum		<= w_SUM;
	o_carry 	<= w_CARRY(4);
	
end STRUCTURAL; 